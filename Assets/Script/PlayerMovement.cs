using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float jumPower;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private LayerMask wallLayer;
    private Rigidbody2D rb;
    private Animator anim;
    private BoxCollider2D boxCollider;
    private float wallCooldown;
    private float horizontal;

    private void Awake() 
    {
        rb = GetComponent<Rigidbody2D>();   
        anim = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    private void Update() 
    {  
        horizontal = Input.GetAxis("Horizontal");
    
        //flip tranform Movement
        if(horizontal > 0.01f)
        {
            transform.localScale = Vector3.one;
        }
         else if(horizontal < -0.01f)
        {
            transform.localScale = new Vector3(-1,1,1);
        }
       
        anim.SetBool("Run" , horizontal != 0);
        anim.SetBool("grounded" , isGrounded());

        //wall jump cooldown
        if(wallCooldown < 0.2f)
        { 
            rb.velocity = new Vector2(horizontal * speed,rb.velocity.y);

            if(OnWall() && !isGrounded())
                {
                    rb.gravityScale = 0;
                    rb.velocity = Vector2.zero;
                }
                else
                {
                    rb.gravityScale = 3f;
                }
                if(Input.GetKey(KeyCode.Space))
                    {
                        Jump();
                    }
        }
        else
        {
            wallCooldown += Time.deltaTime;
        }
    }
    //jump + speed same value
    private void Jump()
    {
        if(isGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x ,jumPower);
            anim.SetTrigger("jump");     
        }
        else if(OnWall() && !isGrounded())
        {
            if(horizontal == 0)
            {
                rb.velocity = new Vector2(-Mathf.Sign(transform.localScale.x)*10,0);
                transform.localScale = new Vector3(-Mathf.Sign(transform.localScale.x),transform.localScale.y,transform.localScale.z);
            }
            else
            {
                rb.velocity = new Vector2(-Mathf.Sign(transform.localScale.x)*3,6);
            }
            wallCooldown = 0; 
        }   
    }
    //check ground raycast
    private bool isGrounded()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center,boxCollider.bounds.size,0,Vector2.down,0.1f,groundLayer);
        return raycastHit.collider != null;
    }
    //check wall raycast
    private bool OnWall()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center,boxCollider.bounds.size,0,new Vector2(transform.localScale.x,0),0.1f,wallLayer);
        return raycastHit.collider != null;
    }

    public bool canAttack()
    {
        return horizontal == 0 && isGrounded() && !OnWall();
    }


}
