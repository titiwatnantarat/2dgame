using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField]private float attackCooldown;
    [SerializeField]private Transform firePoint;
    [SerializeField]private GameObject[] bullet;
    [SerializeField]private AudioClip fireBullet;

    private Animator anim;
    private PlayerMovement playerMovement;
    private float cooldownTime = Mathf.Infinity;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    private void Update()
    {
        if(Input.GetMouseButton(0) && cooldownTime > attackCooldown && playerMovement.canAttack())
        {
            Attack();
        }
        cooldownTime += Time.deltaTime;
    }

    private void Attack()
    {
        SoundManager.instance.PlaySound(fireBullet);
        anim.SetTrigger("attack");
        cooldownTime = 0;

        bullet[FindBullet()].transform.position = firePoint.position;
        bullet[FindBullet()].GetComponent<Projectile>().SetDirection(Mathf.Sign(transform.localScale.x));
    }

    private int FindBullet()
    {
        for(int i = 0; i < bullet.Length; i++)
        {
            if(!bullet[i].activeInHierarchy)
            {
                return i;
            }    
        }
        return 0;
    }
}
