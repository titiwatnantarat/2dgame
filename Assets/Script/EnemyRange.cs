using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRange : MonoBehaviour
{
    [SerializeField] private float attackCooldown;
    [SerializeField] private float range;
    [SerializeField] private float colliderDistance;
    [SerializeField] private int damage;
    [SerializeField] private BoxCollider2D boxCollider;
    [SerializeField] private LayerMask playerLayer;
    [Header("Ranged")]
    [SerializeField] private Transform firepoint;
    [SerializeField] private GameObject[] firebullet;
    private float cooldownTime = Mathf.Infinity;
    private Animator anim;
    private EnemyPatrol enemyPatrol;
    

    private void Awake()
    {
        anim = GetComponent<Animator>();
        enemyPatrol = GetComponentInParent<EnemyPatrol>();
    }
    private void Update()
    {
        cooldownTime += Time.deltaTime;
        if(PlayerInSight())
        {
             if(cooldownTime >= attackCooldown)
            {
                cooldownTime = 0;
                anim.SetTrigger("attack");
            }
        }

        if(enemyPatrol != null)
        {
            enemyPatrol.enabled = !PlayerInSight();
        }
       
    }

    private void RangeAttack()
    {
        cooldownTime = 0;
        firebullet[FindFirebullet()].transform.position = firepoint.position;
        firebullet[FindFirebullet()].GetComponent<EnemyProjectile>().ActivateProjectile();
    }
    private int FindFirebullet()
    {
        for (int i = 0; i < firebullet.Length; i++)
        {
            if(!firebullet[i].activeInHierarchy)
            {
                return i;
            }
        }
        return 0;
    }
     private bool PlayerInSight()
    {
        RaycastHit2D hit = Physics2D.BoxCast(boxCollider.bounds.center + transform.right * range * transform.localScale.x
        * colliderDistance,new Vector3(boxCollider.bounds.size.x * range,boxCollider.bounds.size.y,boxCollider.bounds.size.z
        ),0,Vector2.left,0,playerLayer);
        
        return hit.collider != null;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boxCollider.bounds.center + transform.right * range * transform.localScale.x * colliderDistance ,
        new Vector3(boxCollider.bounds.size.x * range,boxCollider.bounds.size.y,boxCollider.bounds.size.z));
    }

    

}
